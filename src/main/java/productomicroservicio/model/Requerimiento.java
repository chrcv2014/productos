package productomicroservicio.model;

public class Requerimiento {
    long productoID;
    long tipoInsumoID;
    int cantidad;


    public Requerimiento(long productoID, long tipoInsumoID, int cantidad) {
        this.productoID = productoID;
        this.tipoInsumoID = tipoInsumoID;
        this.cantidad = cantidad;
    }

    public Requerimiento() {
    }

    public long getProductoID() {
        return this.productoID;
    }

    public void setProductoID(long productoID) {
        this.productoID = productoID;
    }

    public long getTipoInsumoID() {
        return this.tipoInsumoID;
    }

    public void setTipoInsumoID(long tipoInsumoID) {
        this.tipoInsumoID = tipoInsumoID;
    }

    public int getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

}
