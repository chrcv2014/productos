package productomicroservicio.model;

public class Producto {
    private long productoID;
    private String nombre;
    private int precio;
    private String descripcion;

    public Producto(long productoID, String nombre, int precio, String descripcion) {
        this.productoID = productoID;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
    }

    public Producto() {
    }

    public long getProductoID() {
        return this.productoID;
    }

    public void setProductoID(long productoID) {
        this.productoID = productoID;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return this.precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
