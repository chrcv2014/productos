package productomicroservicio.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import productomicroservicio.model.Requerimiento;



public class RequerimientoMapper implements RowMapper<Requerimiento> {
       @Override
    @Nullable
    public Requerimiento mapRow(ResultSet rs, int rowNum) throws SQLException {
        Requerimiento requerimiento = new Requerimiento();

        requerimiento.setProductoID(rs.getLong("productoID"));
        requerimiento.setTipoInsumoID(rs.getLong("tipoInsumoID"));
        requerimiento.setCantidad(rs.getInt("cantidad"));



        return requerimiento;

    } 
}
