package productomicroservicio.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import productomicroservicio.model.Producto;


public class ProductoMapper implements RowMapper<Producto> {
    @Override
    @Nullable
    public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
        Producto producto = new Producto();

        producto.setProductoID(rs.getLong("productoID"));
        producto.setNombre(rs.getString("nombre"));
        producto.setPrecio(rs.getInt("precio"));
        producto.setDescripcion(rs.getString("descripcion"));


        return producto;

    }
}
