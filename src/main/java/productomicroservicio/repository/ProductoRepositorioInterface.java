package productomicroservicio.repository;

import productomicroservicio.model.Producto;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

public interface ProductoRepositorioInterface extends BaseRepository<Producto> {
        public boolean create(Producto producto);
        public boolean update(Producto producto);
    
        public List<Producto> read(Pageable pageable);
}
