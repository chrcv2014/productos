package productomicroservicio.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import productomicroservicio.mapper.ProductoMapper;
import productomicroservicio.model.Producto;


@Repository
public class ProductoRepository implements ProductoRepositorioInterface{
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Producto readById(int Id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'readById'");
    }



    @Override
    public boolean create(Producto producto) {
        try{
            String sql = "INSERT INTO producto(nombre, precio, descripcion) VALUES (?, ?, ?)";
            Object[] params = new Object[]{producto.getNombre(), producto.getPrecio(), producto.getDescripcion()};

            jdbcTemplate.update(sql, params);
            return true;}
            catch(Exception e){
                e.printStackTrace();
                return false;
            } 
    }

    @Override
    public boolean update(Producto producto) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

 


        @Override
        public List<Producto> read(Pageable pageable) {
          return jdbcTemplate.query("select * from producto", new ProductoMapper());
        }
    
    
}
