package productomicroservicio.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

import productomicroservicio.model.Requerimiento;

public interface RequerimientoRepositorioInterface extends BaseRepository<Requerimiento> {
        public boolean create(Requerimiento requerimiento);
        public boolean update(Requerimiento requerimiento);
    
        public List<Requerimiento> read(Pageable pageable);
}
