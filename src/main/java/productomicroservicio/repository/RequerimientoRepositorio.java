package productomicroservicio.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import productomicroservicio.mapper.RequerimientoMapper;
import productomicroservicio.model.Requerimiento;

@Repository
public class RequerimientoRepositorio implements RequerimientoRepositorioInterface {

        @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Requerimiento readById(int Id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'readById'");
    }

    @Override
    public boolean create(Requerimiento requerimiento) {
        try{
            String sql = "INSERT INTO requerimiento VALUES (?, ?, ?)";
            Object[] params = new Object[]{requerimiento.getProductoID(), requerimiento.getTipoInsumoID(), requerimiento.getCantidad()};

            jdbcTemplate.update(sql, params);
            return true;}
            catch(Exception e){
                e.printStackTrace();
                return false;
            } 
    }

    @Override
    public boolean update(Requerimiento requerimiento) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public List<Requerimiento> read(Pageable pageable) {
          return jdbcTemplate.query("select * from requerimiento", new RequerimientoMapper());

    }
    
}
