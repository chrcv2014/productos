package productomicroservicio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import productomicroservicio.model.Producto;
import productomicroservicio.repository.ProductoRepository;

@Controller
@RequestMapping("/producto")
public class ProductoController {
        @Autowired
    ProductoRepository productoRepository;

    @GetMapping(path = "")
    public String home() {
        return "indexProducto.html";
    }
    @GetMapping(path = "/create")
    public ModelAndView create() {

        ModelAndView modelAndView = new ModelAndView("createProducto.html");
        modelAndView.addObject("producto", new Producto());
        return modelAndView;
    }

    @PostMapping(path = "/create")
    public String created(@ModelAttribute Producto producto) {
        productoRepository.create(producto);

        return "redirect:/producto/create";
    }
    @GetMapping(path = "/read")
    public ModelAndView read(Pageable pageable) {
        ModelAndView modelAndView = new ModelAndView("readProducto.html");
        modelAndView.addObject("producto", productoRepository.read(pageable));
        return modelAndView;
    }
}
