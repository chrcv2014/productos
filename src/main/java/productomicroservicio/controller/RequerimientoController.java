package productomicroservicio.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import productomicroservicio.model.Producto;
import productomicroservicio.model.Requerimiento;
import productomicroservicio.repository.ProductoRepository;
import productomicroservicio.repository.RequerimientoRepositorio;

@Controller
@RequestMapping("/requerimiento")
public class RequerimientoController {
           @Autowired
    RequerimientoRepositorio requerimientoRepositorio;

    @GetMapping(path = "")
    public String home() {
        return "indexRequerimiento.html";
    }
    @GetMapping(path = "/create")
    public ModelAndView create() {

        ModelAndView modelAndView = new ModelAndView("createRequerimiento.html");
        modelAndView.addObject("requerimiento", new Requerimiento());
        return modelAndView;
    }

    @PostMapping(path = "/create")
    public String created(@ModelAttribute Requerimiento requerimiento) {
        requerimientoRepositorio.create(requerimiento);

        return "redirect:/requerimiento/create";
    }
    @GetMapping(path = "/read")
    public ModelAndView read(Pageable pageable) {
        ModelAndView modelAndView = new ModelAndView("readRequerimiento.html");
        modelAndView.addObject("requerimiento", requerimientoRepositorio.read(pageable));
        return modelAndView;
    } 
}
